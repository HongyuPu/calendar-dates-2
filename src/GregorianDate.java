public class GregorianDate extends Date{
    public GregorianDate() { //default constructor
        this.year = 0;
        long remainderDays;
        remainderDays = ((System.currentTimeMillis() + java.util.TimeZone.getDefault().getRawOffset()) / 86400000);
        while (!isLeapYear(this.year) && remainderDays >= 365 || isLeapYear(this.year) && remainderDays >= 366) {
            if (!isLeapYear(this.year)) {
                remainderDays -= 365; //finds the current year by operating through days
                this.year++;
            } else {
                remainderDays -= 366;
                this.year++;
            }
        }
        this.year += 1970; //takes the system time and convertitng it into days than adds it to 1970.

        int monthCurrent = 1;
        for (int i = 1; remainderDays >= getNumberOfDaysInMonth(year, i) && i <= 12; i++) {
            remainderDays = remainderDays - getNumberOfDaysInMonth(year, i); //gets the number of months from remaining days
            monthCurrent++;
        }
        this.month = monthCurrent;

        this.day = (int) remainderDays + 1; //gets the current number of days in the month
    }

    public GregorianDate(int year, int month, int day) { //overloaded constructor
        this.year = year;
        this.month = month;
        this.day = day;
    }

    @Override
    public boolean isLeapYear() {
        if (this.year % 4 == 0 && !(this.year % 100 == 0) || this.year % 400 == 0) {
            return true; //public leap year method for testing
        }
        else {
            return false;
        }
    }

    public boolean isLeapYear(int year) {
        if (this.year % 4 == 0 && !(this.year % 100 == 0) || this.year % 400 == 0) {
            return true; //leap year method for other methods
        }
        else {
            return false;
        }
    }
}
