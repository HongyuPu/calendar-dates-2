public class Date {
    public int year; //final year variable
    public int month; //final month variable
    public int day; //final day variable

    public void subtractDays(int days) {
        int daysSubtracted;
        daysSubtracted = this.day - days;
        while (daysSubtracted < 1) {
            if (month - 1 < 1) {
                daysSubtracted += getNumberOfDaysInMonth(this.year, 12); //calculates months
                this.month = 12;
                this.year--; //increments year
            }
            else {
                daysSubtracted += getNumberOfDaysInMonth(this.year, this.month - 1);
                this.month--; //calculated months without incrementing year.
            }
        }
        this.day = daysSubtracted; //final amount of days
    }

    public void addDays(int days) {
        int daysAdded;
        daysAdded = this.day + days;
        while (daysAdded > getNumberOfDaysInMonth(this.year, this.month)) {
            daysAdded -= getNumberOfDaysInMonth(this.year, this.month); //increments month
            month++;
            while (this.month > 12) { //increments year
                this.month = 1;
                this.year++;
            }
        }
        this.day = daysAdded; //final amount of days
    }

    public void printShortDate() {
        System.out.printf("%d/%d/%d", month, day, year);
    }

    public void printLongDate() {
        System.out.printf("%s %d, %d", getMonthName(month), day, year);
    }

    public int getCurrentYear() {
        return this.year;
    }

    public int getCurrentMonth() {
        return this.month;
    }

    public int getCurrentDayOfMonth() {
        return this.day;
    }

    public String getCurrentMonthName() {
        return getMonthName(this.month); //uses the final month variable in getMonthName method
    }

    public boolean isLeapYear() { //public method for testing
        return false;
    }

    public boolean isLeapYear(int year) { //private method for operations in code
        return false;
    }

    public int getNumberOfDaysInMonth(int year, int month) {
        int numberOfDays = 0; //returns number of days in the month, used in getMonths, addition and subtracting methods
        switch (month) {
            case 1:
                numberOfDays = 31;
                break;
            case 2:
                if (isLeapYear(year)) {
                    numberOfDays = 29;
                }
                else {
                    numberOfDays = 28;
                }
                break;
            case 3:
                numberOfDays =  31;
                break;
            case 4:
                numberOfDays = 30;
                break;
            case 5:
                numberOfDays = 31;
                break;
            case 6:
                numberOfDays = 30;
                break;
            case 7:
                numberOfDays = 31;
                break;
            case 8:
                numberOfDays = 31;
                break;
            case 9:
                numberOfDays = 30;
                break;
            case 10:
                numberOfDays = 31;
                break;
            case 11:
                numberOfDays = 30;
                break;
            case 12:
                numberOfDays = 31;
                break;
            default:
                break;
        }
        return numberOfDays;
    }

    private String getMonthName(int month) { //Return month name used for the getMonthName method
        String monthName = "";
        switch (month) {
            case 1:
                monthName = "January";
                break;
            case 2:
                monthName = "February";
                break;
            case 3:
                monthName = "March";
                break;
            case 4:
                monthName = "April";
                break;
            case 5:
                monthName = "May";
                break;
            case 6:
                monthName = "June";
                break;
            case 7:
                monthName = "July";
                break;
            case 8:
                monthName = "August";
                break;
            case 9:
                monthName = "September";
                break;
            case 10:
                monthName = "October";
                break;
            case 11:
                monthName = "November";
                break;
            case 12:
                monthName = "December";
                break;
            default:
                break;
        }
        return monthName;
    }
}
