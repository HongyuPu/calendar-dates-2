public class JulianDate extends Date{
    public JulianDate() { //default constructor
        this.year = 1;
        long remainderDays;
        remainderDays = 719164 + ((System.currentTimeMillis() + java.util.TimeZone.getDefault().getRawOffset()) / 86400000);
        while (!isLeapYear(this.year) && remainderDays >= 365 || isLeapYear(this.year) && remainderDays >= 366) {
            if (!isLeapYear(this.year)) {
                remainderDays -= 365; //finds the current year by operating through days
                this.year++;
            } else {
                remainderDays -= 366;
                this.year++;
            }
        }

        int monthCurrent = 1;
        for (int i = 1; remainderDays >= getNumberOfDaysInMonth(year, i) && i <= 12; i++) {
            remainderDays = remainderDays - getNumberOfDaysInMonth(year, i);
            monthCurrent++;
        }
        this.month = monthCurrent; //finds the month from the left over days

        this.day = (int) remainderDays + 1; //takes the remaining number of days and sets as final day variable
    }

    public JulianDate(int year, int month, int day) { //overloaded constructor
        this.year = year;
        this.month = month;
        this.day = day;
    }

    @Override
    public boolean isLeapYear() { //public method for testing
        if (this.year % 4 == 0) {
            return true;
        }
        else {
            return false;
        }
    }

    public boolean isLeapYear(int year) { //private method for operations in code
        if (year % 4 ==0) {
            return true;
        }
        else {
            return false;
        }
    }

}
